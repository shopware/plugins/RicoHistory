{extends file="parent:frontend/index/index.tpl"}

{block name='frontend_index_content'}
    {block name='rico_history_frontend_index_content_header'}
        <div class="headline">
            <div class="heading">
                <h1>{s name="headline"}{/s}</h1>
            </div>
            <div class="subheading">
                <p>{s name="subheadline"}{/s}</p>
            </div>
        </div>
    {/block}
    {block name='rico_history_frontend_index_content_wrapper'}
        <div class="history history--wrapper">
            <div class="history--line"></div>
            {$float="milestone--left"}
            {foreach from=$history key=year item=milestones}
                <div class="history--year" id="{$year}">
                    {block name='rico_history_frontend_index_content_year_header'}
                        <div class="year--container">
                            <h2 class="year--text">{$year}</h2>
                        </div>
                    {/block}
                    {foreach from=$milestones key=title item=data}
                        <div class="year--milestone" id="{$title|replace:' ':'_'}">
                            <div class="milestone--pointer"></div>
                            <div class="milestone--content {$float}">
                                {block name='rico_history_frontend_index_content_title'}
                                    <div class="milestone--title">
                                        <h4>{$title}</h4>
                                    </div>
                                {/block}
                                {block name='rico_history_frontend_index_content_description'}
                                    <div class="milestone--description">
                                        {$data['description']}
                                    </div>
                                {/block}
                                {block name='rico_history_frontend_index_content_picture'}
                                    {if $data.image}
                                        <div class="milestone--media">
                                            {if $data.image.thumbnails|@count > 0}
                                                {block name='rico_history_frontend_index_content_picture_thumbnails'}
                                                    <picture>
                                                        <img srcset="{$data.image.thumbnails[0].sourceSet}"
                                                             alt="{$data.description|strip_tags|trim}"
                                                             title="{$data.description|strip_tags|trim|truncate:160}" />
                                                    </picture>
                                                {/block}
                                            {else}
                                                {block name='rico_history_frontend_index_content_plain_image'}
                                                    <img src="{$data.image.source}"
                                                         alt="{$data.description|strip_tags|trim}"
                                                         title="{$data.description|strip_tags|trim|truncate:160}" />
                                                {/block}
                                            {/if}
                                        </div>
                                    {/if}
                                {/block}
                            </div>
                        </div>
                        {if $float === 'milestone--left'}
                            {$float="milestone--right"}
                        {else}
                            {$float="milestone--left"}
                        {/if}
                    {/foreach}
                </div>
            {/foreach}
        </div>
    {/block}
{/block}
{block name='frontend_index_content_left'}{/block}
