// {block name="backend/performance/view/main/multi_request_tasks" append}
Ext.define('Shopware.apps.Performance.view.main.RicoHistory', {
    override: 'Shopware.apps.Performance.view.main.MultiRequestTasks',
    initComponent: function() {
        this.addProgressBar(
            {
                initialText: 'RicoHistory URLs',
                progressText: '[0] of [1] history URLs',
                requestUrl: '{url controller=RicoHistory action=seoHistory}'
            },
            'history',
            'seo'
        );
        this.callParent(arguments);
    }
});
// {/block}
