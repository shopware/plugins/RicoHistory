Ext.define('Shopware.apps.RicoHistory.store.Milestone', {
    extend: 'Shopware.store.Listing',
    model: 'Shopware.apps.RicoHistory.model.Milestone',
    configure: function () {
        return {
            controller: 'RicoHistory'
        };
    }
});
