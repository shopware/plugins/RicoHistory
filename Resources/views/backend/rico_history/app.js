Ext.define('Shopware.apps.RicoHistory', {
    extend: 'Enlight.app.SubApplication',
    name: 'Shopware.apps.RicoHistory',
    loadPath: '{url action=load}',
    bulkLoad: true,
    controllers: ['Main'],
    views: [
        'list.Window',
        'list.Milestone',
        'detail.Milestone',
        'detail.Window'
    ],
    models: ['Milestone'],
    stores: ['Milestone'],
    launch: function () {
        return this.getController('Main').mainWindow;
    }
});
