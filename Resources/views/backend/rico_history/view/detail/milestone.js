// {namespace name=backend/rico_history/view}
Ext.define('Shopware.apps.RicoHistory.view.detail.Milestone', {
    extend: 'Shopware.model.Container',
    padding: 20,
    configure: function () {
        var me = this;
        return {
            controller: 'RicoHistory',
            fieldSets: [
                {
                    title: '',
                    fields: {
                        active: {}
                    }
                },
                {
                    title: '{s name=detail/milestone/content}{/s}',
                    layout: 'fit',
                    fields: {
                        date: {
                            validator: me.validateEmptyString,
                            xtype: 'datefield',
                            submitFormat: 'd.m.Y',
                            value: new Date()
                        },
                        title: {
                            translatable: true
                        },
                        description: {
                            xtype: 'tinymce',
                            translatable: true
                        },
                        mediaId: {
                            xtype: 'shopware-media-field',
                            removeBackground: true
                        }
                    }
                }
            ]
        };
    },
    validateEmptyString: function (value) {
        return !(!value || value === '');
    }
});
