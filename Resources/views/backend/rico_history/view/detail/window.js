// {namespace name=backend/rico_history/view}
Ext.define('Shopware.apps.RicoHistory.view.detail.Window', {
    extend: 'Shopware.window.Detail',
    alias: 'widget.milestone-detail-window',
    title: '{s name=detail/window/window_title}{/s}',
    height: 420,
    width: 900,
    configure: function () {
        return {
            translationKey: 'ricohistory_milestone'
        };
    }
});
