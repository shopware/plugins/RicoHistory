Ext.define('Shopware.apps.RicoHistory.view.list.Milestone', {
    extend: 'Shopware.grid.Panel',
    alias: 'widget.milestone-listing-grid',
    region: 'center',
    configure: function () {
        return {
            detailWindow: 'Shopware.apps.RicoHistory.view.detail.Window',
            columns: {
                active: {},
                date: {},
                title: {},
                description: {}
            }
        };
    }
});
