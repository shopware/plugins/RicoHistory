// {namespace name=backend/rico_history/view}
Ext.define('Shopware.apps.RicoHistory.view.list.Window', {
    extend: 'Shopware.window.Listing',
    alias: 'widget.milestone-list-window',
    height: 450,
    title: '{s name=list/window/window_title}{/s}',
    configure: function () {
        return {
            listingGrid: 'Shopware.apps.RicoHistory.view.list.Milestone',
            listingStore: 'Shopware.apps.RicoHistory.store.Milestone'
        };
    }
});
