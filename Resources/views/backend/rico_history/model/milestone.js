Ext.define('Shopware.apps.RicoHistory.model.Milestone', {
    extend: 'Shopware.data.Model',
    fields: [
        { name: 'id', type: 'int', useNull: true },
        { name: 'active', type: 'boolean' },
        { name: 'date' },
        { name: 'title', type: 'string' },
        { name: 'description', type: 'string', useNull: true },
        { name: 'mediaId', type: 'int' }
    ],
    associations: [
        {
            relation: 'ManyToOne',
            field: 'mediaId',
            type: 'hasMany',
            model: 'Shopware.apps.Base.model.Media',
            name: 'getMedia',
            associationKey: 'mediaAssociation'
        }
    ],
    validations: [
        { field: 'name', type: 'presence' },
        { field: 'date', type: 'presence' }
    ],
    configure: function () {
        return {
            controller: 'RicoHistory',
            detail: 'Shopware.apps.RicoHistory.view.detail.Milestone'
        };
    }
});
