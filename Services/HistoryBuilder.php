<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoHistory\Services;

use RicoHistory\Components\Translation\TranslationOverlayComponent;
use RicoHistory\Models\Milestone;
use RicoHistory\Repository\MilestoneRepository;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Shop\DetachedShop;

class HistoryBuilder
{
    /**
     * @var ModelManager
     */
    private $modelManager;

    /**
     * @var TranslationOverlayComponent
     */
    private $translationComponent;

    /**
     * @var MilestoneRepository
     */
    private $mileStoneRepository;

    /**
     * @var DetachedShop
     */
    private $shop;

    /**
     * @var MediaModelFrontendServiceInterface
     */
    private $mediaModelFrontendService;

    public function __construct(
        ModelManager $modelManager,
        TranslationOverlayComponent $translationComponent,
        MediaModelFrontendServiceInterface $mediaModelFrontendService
    ) {
        $this->modelManager = $modelManager;
        $this->mileStoneRepository = $modelManager->getRepository(Milestone::class);
        $this->translationComponent = $translationComponent;
        $this->mediaModelFrontendService = $mediaModelFrontendService;
    }

    public function mapHistory(DetachedShop $shop): array
    {
        $this->shop = $shop;
        $history = $this->mileStoneRepository->findBy(['active' => true], ['date' => 'ASC']);
        $sorted = [];
        /** @var Milestone $milestone */
        foreach ($history as $milestone) {
            $timestamp = $milestone->getDate()->getTimestamp();
            if (!$this->shop->getDefault()) {
                $translated = $this->translateMilestone($milestone);
                if (!$translated instanceof Milestone) {
                    continue;
                }
                $milestone = $translated;
            }
            $sorted[date('Y', $timestamp)][$milestone->getTitle()] = [
                'description' => $milestone->getDescription(),
                'media' => $milestone->getMedia(),
                'image' => !is_null($milestone->getMedia()) ?
                    $this->mediaModelFrontendService->convert($milestone->getMedia()->getId()) : null,
            ];
        }

        return ['history' => $sorted];
    }

    private function translateMilestone(Milestone $milestone): ?Milestone
    {
        $translated = $this->translationComponent->translate(
            $this->shop->getId(),
            [$milestone],
            Milestone::TRANSLATION_KEY
        );

        return $translated[0] ?? null;
    }
}
