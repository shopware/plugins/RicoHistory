<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoHistory\Components\Manager;

use Exception;
use Shopware\Components\Model\ModelManager;
use Shopware\Components\Plugin\CachedConfigReader;
use Shopware\Models\Shop\Shop;
use Shopware_Components_Modules;
use sRewriteTable;

class SeoUrlManager implements SeoUrlManagerInterface
{
    protected const ORIGINAL_PATH = 'sViewport=RicoHistory';

    /**
     * @var Shopware_Components_Modules
     */
    protected $modules;

    /**
     * @var ModelManager
     */
    protected $modelManager;

    /**
     * @var CachedConfigReader
     */
    protected $configReader;

    /**
     * @var string
     */
    protected $pluginName = '';

    /**
     * @var sRewriteTable
     */
    protected $rewriteTable;

    public function __construct(
        Shopware_Components_Modules $modules,
        ModelManager $modelManager,
        CachedConfigReader $configReader,
        string $pluginName
    ) {
        $this->modules = $modules;
        $this->modelManager = $modelManager;
        $this->configReader = $configReader;
        $this->pluginName = $pluginName;
        $this->rewriteTable = $this->modules->RewriteTable();
    }

    public function refreshSeoUrls(Shop $shop): void
    {
        $this->flushExistingUrls($shop->getId());
        $this->buildUrlsForShop($shop);
    }

    protected function flushExistingUrls(int $shopId): void
    {
        $this->modelManager->getConnection()->query(
            'DELETE FROM s_core_rewrite_urls WHERE org_path = "' . self::ORIGINAL_PATH . '" AND subshopID = ' . $shopId
        )->execute();
    }

    protected function buildUrlsForShop(Shop $shop): void
    {
        $config = $this->configReader->getByPluginName($this->pluginName, $shop);
        if (!array_key_exists('basePath', $config)) {
            throw new Exception('Could not find required option "basePath" of plugin ' . $this->pluginName . '.');
        }
        $this->insertUrl($shop, self::ORIGINAL_PATH, $config['basePath']);
    }

    private function insertUrl(Shop $shop, string $originalPath, string $seoUrl): void
    {
        $shop->registerResources();
        $seoUrl = $this->rewriteTable->sCleanupPath($seoUrl);
        $this->rewriteTable->sInsertUrl($originalPath, $seoUrl);
    }
}
