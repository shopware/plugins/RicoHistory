<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoHistory\Subscriber;

use Enlight\Event\SubscriberInterface;
use Enlight_Controller_ActionEventArgs;
use Enlight_Event_EventArgs;

class BackendPerformanceSubscriber implements SubscriberInterface
{
    /**
     * @var string
     */
    protected $pluginDirectory = '';

    public function __construct(string $pluginDirectory)
    {
        $this->pluginDirectory = $pluginDirectory;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'Enlight_Controller_Action_PostDispatch_Backend_Performance' => 'onDispatchPerformance',
            'Shopware_Controllers_Seo_filterCounts' => 'addHistoryCount',
        ];
    }

    public function onDispatchPerformance(Enlight_Controller_ActionEventArgs $args): void
    {
        $subject = $args->getSubject();
        $request = $subject->Request();
        if ($request->getActionName() !== 'load') {
            return;
        }
        $subject->View()->addTemplateDir($this->pluginDirectory . '/Resources/views/');
        $subject->View()->extendsTemplate('backend/performance/view/main/history.js');
    }

    public function addHistoryCount(Enlight_Event_EventArgs $args): array
    {
        $counts = $args->getReturn();
        $counts['history'] = 1;

        return $counts;
    }
}
