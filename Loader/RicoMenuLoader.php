<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoHistory\Loader;

use Doctrine\ORM\EntityManager;
use RicoCustomNavigation\Services\NavigationTypeService;

class RicoMenuLoader
{
    /**
     * Defines how the grid-entries are build and accessed
     */
    const GRID_TYPE = [
        'typeArgs' => [
            'filterBy' => [],
            'model' => '',
            'displayField' => '',
            'typeName' => 'History',
            'parameter' => [],
        ],
        'dataArgs' => [
            'type' => 'internal',
            'linkTarget' => '',
            'niceUrl' => true,
            'controllerName' => 'RicoHistory',
            'actionName' => '',
            'parameter' => [],
        ],
    ];

    /**
     * @var EntityManager
     */
    private $manager;

    /**
     * @var NavigationTypeService
     */
    private $typeService;

    public function __construct(EntityManager $manager, NavigationTypeService $typeService)
    {
        $this->manager = $manager;
        $this->typeService = $typeService;
    }

    public function registerEntry(): void
    {
        $this->typeService->registerType(self::GRID_TYPE['typeArgs'], self::GRID_TYPE['dataArgs']);
    }

    public function deRegisterEntry(): void
    {
        $this->typeService->deRegisterType(self::GRID_TYPE['typeArgs']['typeName']);
    }
}
