<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

class Shopware_Controllers_Backend_RicoHistory extends Shopware_Controllers_Backend_Application
{
    /**
     * @var string
     */
    protected $model = \RicoHistory\Models\Milestone::class;
    /**
     * @var string
     */
    protected $alias = 'milestone';

    public function updateAction()
    {
        // Small workaround to reset media.
        $params = $this->Request()->getParams();
        if (!isset($params['mediaId']) || $params['mediaId'] === 0) {
            $params['media'] = null;
        }
        $this->View()->assign($this->save($params));
    }

    public function seoHistoryAction(): void
    {
        @set_time_limit(1200);
        $shopId = (int) $this->Request()->getParam('shopId');
        /** @var Shopware_Components_SeoIndex $seoIndex */
        $seoIndex = $this->container->get('SeoIndex');
        $shop = $seoIndex->registerShop($shopId);
        /** @var Shopware_Components_Modules $modules */
        $modules = $this->container->get('modules');
        $rewriteTable = $modules->RewriteTable();
        $rewriteTable->baseSetup();
        /** @var \RicoHistory\Components\Manager\SeoUrlManagerInterface $seoUrlManager */
        $seoUrlManager = $this->container->get('rico_history.service.components.manager.seo_url_manager');
        $seoUrlManager->refreshSeoUrls($shop);
        $this->View()->assign([
            'success' => true,
        ]);
    }
}
