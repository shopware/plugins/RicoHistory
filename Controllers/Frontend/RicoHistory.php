<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

use RicoHistory\RicoHistory;
use RicoHistory\Services\HistoryBuilder;
use Shopware\Components\Plugin\CachedConfigReader;
use Shopware\Models\Shop\DetachedShop;

class Shopware_Controllers_Frontend_RicoHistory extends Enlight_Controller_Action
{
    public function indexAction(): void
    {
        /** @var DetachedShop $shop */
        $shop = $this->container->get('shop');
        $config = $this->getCachedConfigReader()->getByPluginName(RicoHistory::PLUGIN_NAME, $shop);
        if (!in_array($shop->getId(), (array) $config['allowedShops'])) {
            $this->forward('pageNotFoundError', 'error', 'frontend');

            return;
        }
        $this->view->assign($this->getHistoryBuilder()->mapHistory($shop));
    }

    private function getHistoryBuilder(): HistoryBuilder
    {
        /** @var HistoryBuilder $service */
        $service = $this->container->get('rico_history.service.history_builder');

        return $service;
    }

    private function getCachedConfigReader(): CachedConfigReader
    {
        /** @var CachedConfigReader $reader */
        $reader = $this->container->get('shopware.plugin.cached_config_reader');

        return $reader;
    }
}
